# coding=utf-8
#####################     SIOMA S.A.S    ############################
#                Codigo de inicializacion de sigma
#####################################################################
######               Importacion de librerias                 #######
import os
import socket
from subprocess import PIPE, Popen
import datetime
import urllib.request, urllib.parse, urllib.error
import psycopg2, psycopg2.extensions, psycopg2.extras
import requests

DEBUG = False
EXECUTE_PATCH = True

######           Datos de version de base de datos            #######
VERSIONDB = 5.2
GITHUBSQLREPO = "sudo git clone -b " + str(
    VERSIONDB) + " --depth 1 https://CristianRojasCTO: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@github.com/Sioma-app/sql_sigma.git"
SQLREPO = "sql_sigma"
TABLAS = ['errors','esquema_cintas', 'tipo_cintas', 'personas', 'staff', 'motivo_rechazos', 'lotes', 'estomas', 'eliminacion_viajes',
          'viajes', 'racimitos', 'eliminacion_racimitos', 'rechazos', 'perfil_racimos', 'fincas', 'validacions','wifis']


######           Datos de version de repositorio web          #######
VERSION_LOCAL_WEB_SERVER = 6.1
GITHUBWEBREPO = "sudo git clone -b " + str(
    VERSION_LOCAL_WEB_SERVER) + " --depth 1 https://CristianRojasCTO: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@github.com/Sioma-app/sigmaweb.git"
WEBREPO = "sigmaweb"
WEBPATH = "/var/www/html"

######           Datos de version de repositorio py           #######
VERSIONPY = 3.14
GITHUBPYREPO = "sudo git clone -b " + str(
    VERSIONPY) + " --depth 1 https://CristianRojasCTO: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@github.com/Sioma-app/codes_sigma.git"
PYREPO = "codes_sigma"
PYPATH = "/firmware/base/Codes"


######         Declaracion de variables y constantes          #######
online = False
date = datetime.datetime.now()
date = str(date.date())
print(("Fecha actual: " + date))
f = open("/firmware/base/Codes/initbascula/info.txt", "r")
parametrosLocales = str.split(f.readline(), ',')
SERIAL = parametrosLocales[0]


######                        Funciones                        #######
def Cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]


###### Funcion de creacion de cursor
def Create_cursor(json=False):
    if json:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')
        conectar.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ)
        cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)
    else:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')
        conectar.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ)
        cursor = conectar.cursor()
    return cursor, conectar

#### Funcion para ingresar o actualizar parametro en la base de datos
def Set_parametro(parametro, valor):
    fecha = Cmd_line("date")  # Actualizar la fecha a almacenar
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT * FROM parametros")
    recs = cursor.fetchall()
    rows = [dict(rec) for rec in recs]
    existe = False
    print(parametro)
    for i in range(0, len(rows)):
        if rows[i]['parametro'] == parametro:
            id = rows[i]['parametro_id']
            existe = True
    try:
        if existe:
            cursor.execute("update parametros set valor=%s, fecha=%s where parametro_id=%s;", (valor, fecha, id))
            print(("Paramretro ", parametro, "Actualizado"))
        else:
            cursor.execute("insert into parametros (parametro, fecha, valor) values (%s, %s, %s)", (parametro, fecha, valor))
            print(("Paramretro ", parametro, "Actualizado"))

        conectar.commit()
    except Exception as e:
        print(("ERROR ACTUALIZANDO PARAMETRO ", parametro, ": ", repr(e)))

def Web_noti(subject, datos, timeout=6, intentos=1):
    print(("Subject: ", subject))
    print(("Datos a enviar: ", datos))
    SIOMAPPCREDENTIALS = "C0l0mb14S10m4"
    mydata = [('s', SIOMAPPCREDENTIALS), ('subject', subject), ('body', datos)]
    mydata = urllib.parse.urlencode(mydata)
    path = 'https://filtro.siomapp.com/1/correoAlertaStaff'
    header = {"Content-type": "application/x-www-form-urlencoded", "Authorization": "gS8Jdz24bJgevS9loGuiCW9R2IJXLcpdme8nQ8b88No"}
    ok = False
    for i in range(0, intentos):
        i += 1
        if not ok:
            try:
                page = requests.post(path, data=mydata, headers=header, timeout=timeout)
                return page.json()
                ok = True
            except Exception as e:
                print(("ERROR EN LA CONEXION A SIOMA: " + repr(e)))
                return 0
        else:
            break


def Clonar_repo(repo, repoWeb, emailSubject, emailBody, maxIntentosAct=1, copy=True):
    intentos = 0
    descargar = 1
    while intentos < maxIntentosAct:
        ##### Se borra la carpeta local del repositorio
        try:
            borrar = os.system('sudo rm -R ' + repo)
        except Exception as e:
            msj = "No se borro el repositorio: "
            print((msj + repr(e)))
            Web_noti(emailSubject, emailBody + msj)
            break
        print(("borrar: ", borrar))

        try:
            descargar = os.system(repoWeb)
        except Exception as e:
            msj = "No se descargo el repositorio: "
            print((msj + repr(e)))
            Web_noti(emailSubject, emailBody + msj)
        print(("descargar: ", descargar))

        if descargar != 0:
            break
        if copy:
            try:
                copiar = os.system('sudo cp ' + repo + ' -R /firmware/base/Codes')
            except Exception as e:
                msj = "No se copio el repositorio: "
                print((msj + repr(e)))
                Web_noti(emailSubject, emailBody + msj)
            print(("Copiar: ", copiar))

        intentos += 1
        if copy:
            if descargar == 0 and copiar == 0:
                return 0
        else:
            if descargar == 0:
                return 0

######           Validacion de acceso a internet               #######
try:
    socket.setdefaulttimeout(8)
    socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect(("8.8.8.8", 53))
    print('System Online')
    online = True
except Exception as e:
    print('System Offline')
    print(("ERROR TESTING INTERNET: " + repr(e)))
    online = False

### Actualizacion de hora
hora = Cmd_line("date")
print(("Hora sistema: ", hora))
rtc = Cmd_line("sudo hwclock -r")
print(("RTC: ", rtc))
if not online:
    print("Sistema sin internet, se usara hora de RTC")
    Cmd_line("sudo hwclock -s")

######################################################################
#######           Bloque de configuraciones del OS           #########
if EXECUTE_PATCH:
    Cmd_line("sudo chmod 755 /firmware/base/Codes/boot_sigma/patch.sh")
    Cmd_line("sudo /firmware/base/Codes/boot_sigma/patch.sh")




#######           Descarga de esquema de base de datos         #######
try:
    os.system('sudo chown pi /firmware -R')
    sqlBackup = os.system("sudo PGPASSWORD='sioma' pg_dump -U postgres -F t estomadb > "
                    "/firmware/base/Codes/" + date)
except Exception as e:
    print(("ERROR CREANDO COPIA DE BASE DE DATOS: " + repr(e)))
    asunto = "Error de actualizacion de esquema"
    body = "El equipo " + SERIAL + " presenta error al realizar el respaldo de la base de datos."
    Web_noti(asunto, body)
# si se realizo el backup de base de datos con exito, actualice
if sqlBackup == 0:
    try:
        drop = 0
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')
        conectar.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ)
        cursor = conectar.cursor()
        cursor.execute("select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';")
        aux = cursor.fetchall()
        if len(aux) > 1:
            tablas = []
            for tabla in aux:
                tablas.append(tabla[0])
            if "parametros" in tablas:
                tablas.remove("parametros")

            dropCommand = "sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -c 'drop table "
            first = True
            for tabla in tablas:
                if first:
                    first = False
                    dropCommand = dropCommand + tabla
                else:
                    dropCommand = dropCommand + ", " + tabla

            dropCommand = dropCommand + "'"
            drop = os.system(dropCommand)
        else:
            drop = 0

    except Exception as e:
        print(("ERROR BORRANDO SCHEMA: " + repr(e)))
        asunto = "Error de actualizacion de esquema"
        body = "El equipo " + SERIAL + " presenta un error borrando el schema de la base de datos."
        Web_noti(asunto, body)
    if drop == 0:
        # Clonar sql
        try:
            clonar = Clonar_repo(SQLREPO, GITHUBSQLREPO, "Error de actualizacion de esquema",
                        "El equipo " + SERIAL + " presenta un error descargando el archivo sql.")
        except Exception as e:
            print(("ERROR DESCARGANDO .SQL: " + repr(e)))
            ### Ejecutar comando cuando clonar = 1
            os.system(
                "sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -a -f "
                "/firmware/base/Codes/sql_sigma/sql_sigma.sql >/dev/null 2>&1")

        # Si se descargo el repositorio de github
        if clonar == 0:
            try:
                clonarWeb = Clonar_repo(WEBREPO, GITHUBWEBREPO, "Error de actualizacion de repositorio web",
                                     "El equipo " + SERIAL + " presenta un error descargando el repositorio web",
                                     copy=False)
            except Exception as e:
                print(("ERROR ACTUALIZANDO CODIGO WEB: " + repr(e)))
            if clonarWeb == 0:
                # Clonar repo python
                try:
                    clonarPy = Clonar_repo(PYREPO, GITHUBPYREPO, "Error de actualizacion de repositorio py",
                                         "El equipo " + SERIAL + " presenta un error descargando el repositorio py")
                    if clonarPy != 0:
                        print("Fallo clonando codigo python desde web")

                except Exception as e:
                    print(("ERROR ACTUALIZANDO CODIGO py: " + repr(e)))
                    asunto = "Error de actualizacion de py"
                    body = "El equipo " + SERIAL + " presenta error al realizar la actualizacion de py."
                    Web_noti(asunto, body)
            else:
                print("Fallo clonando sigmaweb desde web")
                clonarWeb = 1
                clonarPy = 1
        else:
            clonarWeb = 1
            clonarPy = 1

        print(("clonacionBD: ", clonar, "clonacionPy: ", clonarPy, "clonacionweb: ", clonarWeb))
        if (clonar == 0) and clonarPy == 0 and clonarWeb == 0:
            try:
                if DEBUG:
                    execute = os.system("sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -a -f /firmware/base/Codes/sql_sigma/sql_sigma.sql")
                else:
                    execute = os.system("sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -a -f /firmware/base/Codes/sql_sigma/sql_sigma.sql >/dev/null 2>&1")
                cursor = conectar.cursor()
                cursor.execute("select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';")
                aux = cursor.fetchall()
                tablas = []
                for tabla in aux:
                    tablas.append(tabla[0])

                if "parametros" in tablas:
                    tablas.remove("parametros")
                if "backups" in tablas:
                    tablas.remove("backups")

                print("Tablas descargadas: ",len(tablas))
                print(tablas,"/n")
                print("Tablas por defecto en bootloader: ",len(TABLAS))
                print(TABLAS)

                if len(TABLAS) == len(tablas):
                    br = False # cambiar nombre de variable ( variable para salirse del ciclo superior)
                    checks = []
                    for tablaVersion in TABLAS:
                        for tabla in tablas:
                            if tabla == tablaVersion:
                                checks.append(1)
                                break
                            elif tabla == tablas[len(tablas)-1]:
                                asunto = "Error de actualizacion de esquema"
                                body = "El equipo " + SERIAL + " no paso la validacion de tablas."
                                Web_noti(asunto, body)
                                print("check:SQL:False")
                                print("Tablas no coinciden en repo local y web")
                                br = True
                                break
                        if br:
                            break

                    if len(checks) == len(TABLAS):
                        print("check:SQL:True")
                else:
                    asunto = "Error de actualizacion de esquema"
                    body = "El equipo " + SERIAL + " no paso la validacion de tablas."
                    Web_noti(asunto, body)
                    print("check:SQL:False")
                    print("Numero de tablas no coinciden en repo local y web")

            except Exception as e:
                print(("ERROR GENERANDO NUEVO ESQUEMA: " + repr(e)))
        else:
            print("check:SQL:False")
            print("Fallo en clonación de repo SQL desde web")
    else:
        print("check:SQL:False")
        print("Fallo en drop tables")
else:
    print("check:SQL:False")
    print("Fallo en sqlBackup")

Set_parametro("error", 0)

# ######                Descarga repositorio web                #######
resultado = "check:WEB:False"
if (clonar == 0) and clonarPy == 0 and clonarWeb == 0:
    try:
        copiar = os.system('sudo cp ' + WEBREPO + ' -R ' + WEBPATH)
    except Exception as e:
        msj = "No se copio el repositorio: "
        print((msj + repr(e)))
        Web_noti("Error de actualizacion de repositorio web",
                 "El equipo " + SERIAL + " presenta un error copiando el repositorio web" + msj)
    print(("Copiar: ", copiar))

    if copiar == 0:
        restart = os.system('sudo service apache2 restart')

        if restart == 0:
            SiomappCredentials = "C0l0mb14S10m4"
            URL = 'http://localhost/aCliente/versionSigmaWeb.php' #CONSTANTES GLOBALES, Apellido de las constantes
            try:
                page = requests.get(URL)
                versionWeb = float(page.content)
            except Exception as e:
                versionWeb = 0
                print(("ERROR EN LA CONEXION A SIOMA: " + repr(e)))

            if versionWeb == VERSION_LOCAL_WEB_SERVER:
                resultado = "check:WEB:True"

print(resultado)


######                Descarga repositorio py                 #######

if (clonar == 0) and clonarPy == 0 and clonarWeb == 0:
    try:
        copiar = os.system('sudo cp ' + PYREPO + ' -R ' + PYPATH)
    except Exception as e:
        msj = "No se copio el repositorio: "
        print((msj + repr(e)))
        Web_noti("Error de actualizacion de repositorio py",
                 "El equipo " + SERIAL + " presenta un error copiando el repositorio py" + msj)
    print(("Copiar: ", copiar))

    if copiar == 0:

        import sys
        sys.path.insert(0, '/firmware/base/Codes/codes_sigma')
        import credentials
        versionPy = credentials.Get_Estoma_Info()
        versionPy = versionPy[1]

        if versionPy == VERSIONPY:
            print("check:PY:True")
        else:
            print("check:PY:False")
            print("Versiones de python en Bootloader y credenciales no coinciden")
    else:
        print("check:PY:False")
        print("Fallo en copia de archivos python")
else:
    print("check:PY:False")

